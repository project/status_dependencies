<?php

namespace Drupal\status_dependencies\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\status_dependencies\StatusDependenciesManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Returns responses for Status dependencies routes.
 */
class StatusDependenciesController extends ControllerBase {

  /**
   * Used to get all dependencies needed information.
   *
   * @var \Drupal\status_dependencies\StatusDependenciesManager
   */
  protected StatusDependenciesManager $manager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\status_dependencies\StatusDependenciesManager $manager
   *   The status dependencies manager.
   */
  public function __construct(StatusDependenciesManager $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('status_dependencies.manager'),
    );
  }

  /**
   * Builds the status and the download button.
   */
  public function build() {

    $build['modules'] = $this->buildDetails($this->t('Modules'), $this->manager->getModules());

    $build['themes'] = $this->buildDetails($this->t('Themes'), $this->manager->getThemes());

    $build['download'] = [
      '#type' => 'link',
      '#url' => Url::fromRoute('status_dependencies.report_download'),
      '#title' => $this->t('Download'),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
          'button--small',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Show a details section for a specific category.
   *
   * @param string $category
   *   Category: modules, themes, etc.
   * @param array $data
   *   List of arrays with name and version.
   */
  protected function buildDetails(string $category, array $data) {
    return [
      '#type' => 'details',
      '#title' => $category,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'table' => [
        '#theme' => 'table',
        '#header' => [
          $this->t('Title'),
          $this->t('Machine name'),
          $this->t('Version'),
        ],
        '#rows' => $data,
      ],
    ];
  }

  /**
   * Download the data in CSV format.
   */
  public function download() {
    $modules = array_map(function (array $module) {
      return $module + ['category' => 'module'];
    }, $this->manager->getModules());

    $themes = array_map(function (array $theme) {
      return $theme + ['category' => 'theme'];
    }, $this->manager->getThemes());

    $data = array_values(array_merge($modules, $themes));

    // phpcs:ignore
    // From: http://obtao.com/blog/2013/12/export-data-to-a-csv-file-with-symfony/
    $response = new StreamedResponse(function () use ($data) {
      $handle = fopen('php://output', 'r+');

      fputcsv($handle, [
        'title',
        'name',
        'version',
        'category',
      ]);

      foreach ($data as $row) {
        fputcsv($handle, $row);
      }

      fclose($handle);
    });

    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="status-dependencies-' . date('Y-m-d') . '.csv"');
    return $response;
  }

}
