<?php

namespace Drupal\status_dependencies;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * Service description.
 */
class StatusDependenciesManager {

  /**
   * Get the list of modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Used to get the list of themes.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs a Manager object.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   Theme handler.
   */
  public function __construct(ModuleExtensionList $moduleExtensionList, ThemeHandlerInterface $themeHandler) {
    $this->moduleExtensionList = $moduleExtensionList;
    $this->themeHandler = $themeHandler;
  }

  /**
   * Get the list of modules.
   *
   * @return array
   *   Module list.
   */
  public function getModules() {
    return $this->getExtensionsData($this->moduleExtensionList->getList());
  }

  /**
   * Get the list of themes.
   *
   * @return array
   *   Theme list.
   */
  public function getThemes() {
    return $this->getExtensionsData($this->themeHandler->listInfo());
  }

  /**
   * Filter and extracts the needed data for extensions.
   *
   * Only extensions enabled and with version are added,
   * the rest ones are filtered out.
   *
   * @param array $extensions
   *   Extension list.
   *
   * @return array
   *   Each array contains the name and the module version.
   */
  protected function getExtensionsData(array $extensions) {
    $extensions_enabled = array_filter($extensions, function (Extension $extension) {
      return $extension->status === 1 && !empty($extension->info['version']);
    });

    return array_values(array_map(function (Extension $extension) {
      return [
        'title' => $extension->info['name'],
        'name' => $extension->getName(),
        'version' => $extension->info['version'] ?? '',
      ];
    }, $extensions_enabled));
  }

}
